package com.example.weatherapp;

import com.example.weatherapp.Model.MainObjectData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiSewrvices {
    @GET("weather?")
    Call<MainObjectData> getData(@Query("q") String cityname, @Query("appid")String appid);
}
