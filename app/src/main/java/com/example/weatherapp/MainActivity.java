package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.weatherapp.Model.MainObjectData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView addressTxt, updated_atTxt, statusTxt, tempTxt, temp_minTxt, temp_maxTxt, sunriseTxt,
            sunsetTxt, windTxt, pressureTxt, humidityTxt,errotTxt;
    ProgressBar progressBar;
    RelativeLayout relativeLayout;
    public  static  final  String   CITY="Dhaka";
    public  static  final  String API="8416c28acc0b5b0b9f57f44917bbee74";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addressTxt = findViewById(R.id.address);
        updated_atTxt = findViewById(R.id.updated_at);
        statusTxt = findViewById(R.id.status);
        tempTxt = findViewById(R.id.temp);
        temp_minTxt = findViewById(R.id.temp_min);
        temp_maxTxt = findViewById(R.id.temp_max);
        sunriseTxt = findViewById(R.id.sunrise);
        sunsetTxt = findViewById(R.id.sunset);
        windTxt = findViewById(R.id.wind);
        pressureTxt = findViewById(R.id.pressure);
        humidityTxt = findViewById(R.id.humidity);
        errotTxt=findViewById(R.id.errorText);


        ApiSewrvices apiSewrvices=RetrofitClient.getRetrofit().create(ApiSewrvices.class);

        apiSewrvices.getData(CITY,API).enqueue(new Callback<MainObjectData>() {
            @Override
            public void onResponse(Call<MainObjectData> call, Response<MainObjectData> response) {
            MainObjectData    data=response.body();

            tempTxt.setText(String.valueOf(data.getMain().getTemp()-273.15+"\u00B0C"));
            statusTxt.setText(data.getWeather().get(0).getMain().toUpperCase());
            Locale c=new Locale("",data.getSys().getCountry());
           addressTxt.setText(String.valueOf(data.getName().toUpperCase())+","+(c.getDisplayCountry()));

                int datee=data.getDt();
                long unix_seconds = datee;
                Date date=new Date(unix_seconds*1000);
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.ENGLISH);
                 String currentdate=simpleDateFormat.format(date);
                 updated_atTxt.setText(currentdate);

                 temp_maxTxt.setText("Max Temp: "+String.valueOf(data.getMain().getTempMax()-273.15+"\u00B0C"));
                temp_minTxt.setText("Min Temp: "+String.valueOf(data.getMain().getTempMin()-273.15+"\u00B0C"));

              //  String sunrise=String.valueOf(data.getSys().getSunrise());

              // sunriseTxt.setText(sunrise);

                Log.d("Dataa", "onResponse: "+response);
            }

            @Override
            public void onFailure(Call<MainObjectData> call, Throwable t) {
                Log.d("Dataa", "onFail: "+t);
            }
        });

    }




}






